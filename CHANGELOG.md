


# 0.5.10

- dc78817 FIX: dont use my_registry_tag

- e2bddb2 FIX: using d3vopz_registry_tag

- 400d16a Merge tag '0.5.9' into develop


# 0.5.9

- 9894ba2 Merge branch 'release/0.5.9'

- 0bd7948 FEAT: update of version 0.5.9

- 968ff48 FEAT: dont abort if ref is not tag. use tag as image tag

- 711165c Merge tag '0.5.8' into develop


# 0.5.8

- ec7f666 Merge branch 'release/0.5.8'

- 5469859 FEAT: update of version 0.5.8

- 56a4d21 FEAT: dont abort if ref is not tag. use tag as image tag

- e186936 FEAT: kaniko build without cache

- 5152c4a Merge tag '0.5.7' into develop


# 0.5.7

- 88f9604 Merge branch 'release/0.5.7'

- 535e307 FEAT: update of version 0.5.7

- a89d3c3 FEAT: RULE#65: only run manifest on linux/arm64

- f9b85c1 FEAT: RULE#65: only run manifest on linux/arm64

- eafed8a Merge tag '0.5.6' into develop


# 0.5.6

- 592cb4f Merge branch 'release/0.5.6'

- d90b326 FEAT: update of version 0.5.6

- f27485b FEAT: trace detail

- 369280b FEAT: use executor path

- f2f1be0 FEAT: use executor path

- db10255 FEAT: use executor path

- ef87e30 FEAT: get executor path

- f7245df Merge tag '0.5.5' into develop


# 0.5.5

- 3bf9fcd Merge branch 'release/0.5.5'

- 568780e FEAT: update of version 0.5.5

- d31283a FEAT: dont reset D3VOPZ_CUSTOM_PLATFORM_TAG, if git tag

- 647cfac Merge tag '0.5.4' into develop


# 0.5.4

- e20a2b9 Merge branch 'release/0.5.4'

- 638dac5 FEAT: update of version 0.5.4

- 82881bd FEAT: use 0.5.2 again

- 127379a Merge tag '0.5.3' into develop


# 0.5.3

- e8bbf70 Merge branch 'release/0.5.3'

- 75d447c FEAT: update of version 0.5.3

- 1fb595f FEAT: use D3VOPZ_DOCKER_REGISTRY

- 38b77d4 Merge tag '0.5.2' into develop


# 0.5.2

- c3eff81 Merge branch 'release/0.5.2'

- 53ec34d FEAT: update of version 0.5.2

- 5c96d13 FEAT: use D3VOPZ_REGISTRY_TAG

- b87efe6 FEAT: use D3VOPZ_REGISTRY_TAG

- e584e67 Merge tag '0.5.1' into develop


# 0.5.1

- 06a90a7 Merge branch 'release/0.5.1'

- a8b3f75 FEAT: update of version 0.5.1

- 6246d26 FEAT: remove common build args

- a2fc385 Merge tag '0.5.0' into develop


# 0.5.0

- 9ff5f82 Merge branch 'release/0.5.0'

- b5b2c62 FEAT: update of version 0.5.0

- 7cc906f Merge branch 'feature/supportPipelineJobs' into develop

- 0fbcbf4 FEAT: armhf -> armv7l

- a0173f0 FEAT: armhf -> arm7l

- 1089fdf FEAT: CUSTOM_ARCH->ARCH

- 986efb7 FEAT: support .build-manifest

- 0305002 FEAT: rename to .build-kaniko

- af552ca FEAT: 1st draft

- 761f008 FEAT: 1st draft

- b8c5b7e FEAT: support multiarch variables

- b378890 Merge tag '0.4.2' into develop


# 0.4.2

- cb36a98 Merge branch 'release/0.4.2'

- a2b1b50 FEAT: update of version 0.4.2

- d1f2bb4 FIX: move into crud

- 5cb7c7f Merge tag '0.4.1' into develop


# 0.4.1

- 9d43cb8 Merge branch 'release/0.4.1'

- f6bbe6d FEAT: update of version 0.4.1

- fd250ca FIX: move into crud

- 6388319 Merge tag '0.4.0' into develop


# 0.4.0

- b713347 Merge branch 'release/0.4.0'

- aa39682 FEAT: update of version 0.4.0

- 62dbb67 FEAT: 1st draft of crud base schema

- a13cb54 FEAT: replace tests

- bc47269 FEAT: replace tests

- 96db9db TRY: local subdir test

- 3a6ebad TRY: local subdir test

- c38c1b7 TRY: local subdir test

- 22b6c4d TRY: local subdir test

- f78b5db TRY: local subdir test

- 44fb131 TRY: local subdir test

- edbc922 TRY: local subdir test

- 55ac4b3 TRY: local subdir test

- 03116ba TRY: local subdir test

- 8c76a49 TRY: local subdir test

- 6b0c693 TRY: local subdir test

- d468657 TRY: local subdir test

- 783c909 TRY: local subdir test

- 1ad1af2 TRY: local subdir test

- e5e581e TRY: local subdir test

- 3ac64a8 TRY: local subdir test

- 133eb9d TRY: local subdir test

- c6c9e4e FIX: trailing spaces

- 5fbc085 FEAT: support D3VOPZ_PROJECT_PATH

- 0d169bc FEAT: support local D3VOPZ_REGISTRY*

- d3fda09 FIX: set D3VOPZ_GITLABRUNNER_TAG

- 0710c9e FIX: set D3VOPZ_GITLABRUNNER_TAG

- 05b1464 FIX: ensure CI_REGISTRY is set

- e3709df Merge tag '0.3.4' into develop


# 0.3.4

- 7479724 Merge branch 'release/0.3.4'

- ecf9fb8 FEAT: update of version 0.3.4

- 10d8674 TRY: show site

- 768e731 TRY:content of public/...

- eacb501 TRY: display content of public, site

- 67f041b Merge tag '0.3.3' into develop


# 0.3.3

- 39b7139 Merge branch 'release/0.3.3'

- b952e21 FEAT: update of version 0.3.3

- 225b129 Merge tag '0.3.2' into develop


# 0.3.2

- d96dafe Merge branch 'release/0.3.2'

- 37678ea FEAT: update of version 0.3.2

- 45ee6ba FIX: remove the execution of pages only on main

- 5266c73 Merge tag '0.3.1' into develop


# 0.3.1

- c8c9604 Merge branch 'release/0.3.1'

- f89835b FEAT: update of version 0.3.1

- 20beb61 FIX: remove ./public access in pages_build

- 4d078cc FIX: changelog_create -> changelog_build , changelog_build is build stage.

- 34abce8 TRY: use site for arefact exchange between jobs, public for caching multiple releases.

- 296004b FEAT: mv mkdocs-template to d3vopz-template

- 3e56787 Merge tag '0.3.0' into develop


# 0.3.0

- 74f04ed Merge branch 'release/0.3.0'

- af6cc96 FEAT: added D3VOPZ_IMAGE_USER description

- cd8757d TRY: kaniko_build

- 0b1b97d FIX: url of sshd-config

- 6797220 FEAT: debian

- 667add6 FEAT: 1st draft of docker

- 4be1bee FEAT: jobs description

- c057ff4 FEAT: description of ci/cd variables

- 1cce63a FIX: removed mapping chapters

- 2ea9e54 FIX: structure  of documentation

- 69d018c FIX: git mandatory

- b8cd5a3 FIX: path to mkdocs.yml.tpl

- 622823f TRY: variable substitution

- a136d5a TRY: change sequence

- 9c8e55d FIX: install macros plugin, ...

- 8f2d94b FIX: add for variables macros; and search

- f322787 FIX: real  site name

- 0978805 TRY: material theme

- 37da03a Merge tag '0.2.5' into develop


# 0.2.5

- 9cad543 Merge branch 'release/0.2.5'

- 5f024e7 FEAT: update of version 0.2.5

- 7758338 FIX: mkdir public

- c12a776 TRY: default

- e8e0cd2 Merge tag '0.2.4' into develop


# 0.2.4

- e429b9b Merge branch 'release/0.2.4'

- 125077f FEAT: update of version 0.2.4

- 47eb724 FIX: pages -> public

- e5398e9 TRY: work on cache

- c8a9cc4 Merge tag '0.2.3' into develop


# 0.2.3

- d3efc74 Merge branch 'release/0.2.3'

- cbe02e2 FEAT: update of version 0.2.3

- ceae02b FIX: typo

- 14ebda4 TRY: caching

- 4e7e2f9 TRY: cache pages

- 0d3b056 TRY: caching

- 87dceff TRY: caching

- c880289 Merge tag '0.2.2' into develop


# 0.2.2

- 4b88fff Merge branch 'release/0.2.2'

- a155efb FEAT: update of version 0.2.2

- a716748 TRY: cache public

- 449b95d Merge tag '0.2.1' into develop


# 0.2.1

- 0703175 Merge branch 'release/0.2.1'

- 555963b FEAT: update of version 0.2.1

- 0d22961 TRY: index.html

- bd5636b TRY: redirect with index.html

- f89331b FIX: project folder

- 05e772c Merge tag '0.2.0' into develop


# 0.2.0

- 8a351ef Merge branch 'release/0.2.0'

- ae8c74a FEAT: update of version 0.2.0

- 20c72b4 TRY: .htaccess

- 62c4462 FEAT: url for pages

- e0e457c FEAT: trace pages url

- 5204d29 TRY: publish to public

- e53a9e1 FIX: folder locations

- fd42352 TRY: 1st draft of build chapters

- 7ff6c1d TRY: avoid venv warning

- 5d978d7 TRY: check mkdocs original

- 2f55a92 FIX: path to index.md

- 68ea541 FIX: update pip

- e6dc3b7 FEAT: 1st draft of tree view

- 1f069a4 Merge tag '0.1.1' into develop


# 0.1.1

- a44b698 Merge branch 'release/0.1.1'

- f80f9e0 FEAT: update of version 0.1.1

- 68001db FIX: describe how to create new minor version

- 5b49f18 FIX: test D3VOPZ_GITLAB_SCRIPT_USER, D3VOPZ_GITLAB_SCRIPT_TOKEN

- 0e87c86 Merge tag '0.1.0' into develop


# 0.1.0

- 9aabc1e Merge branch 'release/0.1.0'

- 0a42a12 FEAT: update of version 0.1.0

- e50e118 FEAT: msg if CHANGELOG.md already exists

- 684c2c9 FEAT: pages needs create_changelog

- 3f196ba FEAT: generate CHANGELOG.md only if not exists

- c857a7d FIX: changelog of local dir

- b570b73 TRY: show changelog

- 796bac6 TRY: remove unused packages

- f2bfe13 FIX: remove openssh ; add git-flow

- d2e6a24 FIX: debian:latest

- bf6ab99 TRY: using debian:slim

- 4b36618 FIX: remove ls -la

- 034263e FIX: add bash

- 527373d TRY: env

- 1619ea8 TRY: 1st call

- b3c98f6 TRY: set path

- 1024e74 TRY: 1st call

- 301bbc5 TRY: 1st call of script

- 53230f7 TRY: 1st call of script

- 2aa03f9 FIX: typo missing

- 21f2610 TRY: clone with credentials

- 6d81ae7 FIX: ssh for git

- 71267c3 FIX: add ssh

- 7a0cdd9 FEAT: local runner; TRY:  git-url

- a45f9a2 TRY: clone with project id d3vopz-base-script

- f6ac841 TRY: support .gitlab-ci in project folder

- bc6ac82 Add new file

- f67f334 Merge tag '0.0.2' into develop


# 0.0.2

- 236e8f1 Merge branch 'release/0.0.2'

- 95a36d0 FEAT: update of version 0.0.2

- 8df79cb FEAT: current changelog

- bad93d6 FEAT: 1st draft of project library

- f1393da FEAT: 1st draft of project files

- c2f923d FEAT: template for sshd_config D3VOPZ_USER_NAME

- 263d6cc Merge tag '0.0.1' into develop


# 0.0.1

- 944daf9 Merge branch 'release/0.0.1' into main

- 17a5f48 FEAT: update

- 822b14d FEAT: added some commands

- a287716 FEAT: 1st creation

- 54d8531 FEAT: remove common

- 07f79d2 FEAT: 1st draft of sshd

- 64139e7 Initial commit
