#!/bin/bash
export D3VOPZ_REPO_PATH=$(git remote -v | grep -o 'git@gitlab.com:[^[:space:]]*' | sed -n 's/.*:\(.*\)\.git$/\1/p')
export D3VOPZ_TARGET_LIST="app vscode"
export D3VOPZ_USER_NAME=d3vopz
export D3VOPZ_USER_GECOS=d3vopz-gecos
export CI_REGISTRY=registry.gitlab.com
export CI_REGISTRY_IMAGE=${CI_REGISTRY}/${D3VOPZ_REPO_PATH}
export CI_PROJECT_DIR=${D3VOPZ_PROJECT_DIR}
export D3VOPZ_GITLABRUNNER_TAG=gitlab.com/d3vopz-public_native
