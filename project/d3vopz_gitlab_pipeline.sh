#!/bin/bash

d3vopz_gitlab_pipeline_install() {
    d3vopz_gitlab_pipeline_yq_install "$@"
}

d3vopz_gitlab_pipeline_yq_install() {
    echo "Installing latest yq..."
    if command -v wget >/dev/null 2>&1; then
        wget -qO- https://github.com/mikefarah/yq/releases/latest/download/yq_linux_amd64.tar.gz | tar xz -C /usr/local/bin
    elif command -v curl >/dev/null 2>&1; then
        curl -sL https://github.com/mikefarah/yq/releases/latest/download/yq_linux_amd64.tar.gz | tar xz -C /usr/local/bin
    else
        echo "eee: NOT_FOUND Neither wget nor curl is installed. Please install one of them to proceed."
        exit 1
    fi
    if [ ! -e /usr/local/bin/yq ]; then
        ln -s /usr/local/bin/yq_linux_amd64 /usr/local/bin/yq
    fi
    chmod +x /usr/local/bin/yq
    local yq_version=$(yq --version)
    echo "iii: PROGRESS.INFO: yq_version=\"$yq_version\" installed successfully."
}   

d3vopz_gitlab_pipeline_init() {
    if ! command -v yq >/dev/null 2>&1; then
        echo "eee: NOT_FOUND yq is not installed. Please install with sudo -E $script_name yq install"
    fi
    # Add commands to initialize iperf
}

d3vopz_gitlab_pipeline_test_run_compose_service() {
    local service_name="$1"
    local file_name="$2"
    local image_full=$(yq e ".services.$service_name.image" "$file_name")
    local image_name=$(echo "$image_full" | cut -d':' -f1)
    local image_tag=$(echo "$image_full" | cut -d':' -f2)
    local build_context=$(yq e ".services.$service_name.build.context" "$file_name")
    local build_dockerfile=$(yq e ".services.$service_name.build.dockerfile" "$file_name")
    local build_target=$(yq e ".services.$service_name.build.target" "$file_name")
    local build_args=$(yq e ".services.$service_name.build.args" "$file_name")
    echo "iii: PROGRESS.INFO: service_name=\"$service_name\"."
    echo "iii: PROGRESS.INFO: image_name=\"$image_name\"."
    echo "iii: PROGRESS.INFO: image_tag=\"$image_tag\"."
    echo "iii: PROGRESS.INFO: build_context=\"$build_context\"."
    echo "iii: PROGRESS.INFO: build_dockerfile=\"$build_dockerfile\"."
    echo "iii: PROGRESS.INFO: build_target=\"$build_target\"."
    build_args=$(echo -e "\n$build_args" | sed 's/^[ \t-]*//')
    for build_arg in $build_args; do
        build_arg_key=$(echo "$build_arg" | cut -d'=' -f1)
        build_arg_value=$(echo "$build_arg" | cut -d'=' -f2)
        echo "iii: PROGRESS.INFO: build_arg_key=\"$build_arg_key\"  build_arg_value=\"$build_arg_value\"."
    done
    echo "iii: PROGRESS.INFO: Starting Kaniko build for service_name=\"$service_name\"."
    if [ -n "$build_target" ]; then
        image_name="$image_name/$build_target"
    fi
    kaniko_args="--context $build_context --dockerfile $build_dockerfile --destination $image_name:$image_tag"
    if [ -n "$build_target" ]; then
        kaniko_args="$kaniko_args --target $build_target"
    fi
    if [ -n "$build_target" ]; then
        kaniko_args="$kaniko_args --target $build_target"
    fi
    for build_arg in $build_args; do
        kaniko_args="$kaniko_args --build-arg $build_arg"
    done
    local kaniko_cmdline="/kaniko/executor $kaniko_args"
    echo "iii: PROGRESS.INFO: kaniko_cmdline=\"$kaniko_cmdline\"."



        
}


d3vopz_gitlab_pipeline_test_run_compose() {
    local file_name="${IPERF_ARG_FILE:-docker-compose.yml}"
    if [ ! -e "$file_name" ]; then
        echo "eee: NOT_FOUND file_name=\"$file_name\"."
        exit 1
    else
        echo "ddd: PROGRESS:INFO: FOUND file_name=\"$file_name\"."
        services=$(yq e '.services | keys' "$file_name")
        if [ -z "$services" ]; then
            echo "eee: NOT_FOUND No services found in file_name=\"$file_name\"."
            exit 1
        else
            services=$(echo -e "\n$services" | sed 's/^[ \t-]*//')
            for service in $services; do
                d3vopz_gitlab_pipeline_test_run_compose_service "$service" "$file_name"
            done
        fi
    fi
}


d3vopz_gitlab_pipeline_test_run() {
    local local_cmd="$1"
    local local_cmd_list="compose"
    if ! echo "$local_cmd_list" | grep -wq "$local_cmd"; then
        echo "eee[$FUNCNAME();$LINENO]: INVALID_ARG local_cmd=\"$local_cmd\""
        exit 1
    fi
    case "$local_cmd" in
        compose)
            shift
            d3vopz_gitlab_pipeline_test_run_compose "$@"
            ;;
        *)
            echo "eee[$FUNCNAME();$LINENO]: INVALID_ARG local_cmd=\"$local_cmd\""
            exit 1
            ;;
    esac
}

d3vopz_gitlab_pipeline_test() {
    local local_cmd="$1"
    local local_cmd_list="run"
    if ! echo "$local_cmd_list" | grep -wq "$local_cmd"; then
        echo "eee[$FUNCNAME();$LINENO]: INVALID_ARG local_cmd=\"$local_cmd\""
    fi
    case "$local_cmd" in
        run)
            shift
            d3vopz_gitlab_pipeline_test_run "$@"
            ;;
        *)
            echo "eee[$FUNCNAME();$LINENO]: INVALID_ARG local_cmd=\"$local_cmd\""
            exit 1
            ;;
    esac
}

#@test:  ./docker-compose/d3vopz_gitlab_pipeline.sh test run compose --file=$PWD/docker-compose/docker-compose.yml
#@test:  ./docker-compose/d3vopz_gitlab_pipeline.sh init
#@test:  sudo -E ./docker-compose/d3vopz_gitlab_pipeline.sh install
d3vopz_gitlab_pipeline_main() {
    script_name="$0"
    local local_cmd="$1"
    for arg in "$@"; do
        if [[ "$arg" == --*=* ]]; then
            param="${arg%%=*}"
            value="${arg#*=}"
            param_name="${param#--}"
            var_name="IPERF_ARG_${param_name^^}"
            declare "$var_name"="$value"
            echo "iii: PROGRESS.INFO: $var_name=\"$value\""
        fi
    done


    local local_cmd_list="init|install|test|status"
    if ! echo "$local_cmd_list" | grep -wq "$local_cmd"; then
        echo "eee[$FUNCNAME();$LINENO]: INVALID_ARG local_cmd=\"$local_cmd\""
        exit 1
    fi
    case "$local_cmd" in
        init)
            shift
            d3vopz_gitlab_pipeline_init "$@"
            ;;
        install)
            shift
            d3vopz_gitlab_pipeline_install "$@"
            ;;
        test)
            shift
            d3vopz_gitlab_pipeline_test "$@"
            ;;
        *)
            echo "eee[$FUNCNAME();$LINENO]: INVALID_ARG local_cmd=\"$local_cmd\""
            exit 1
            ;;
    esac
}

d3vopz_gitlab_pipeline_main "$@"

