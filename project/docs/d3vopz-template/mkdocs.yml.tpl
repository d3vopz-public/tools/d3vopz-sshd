site_name: $D3VOPZ_MKDOCS_SITE_NAME
repo_url: $D3VOPZ_MKDOCS_GIT_URL
theme:
    name: material
    nav: True
plugins:
    - search
    - macros
nav:
  - Home: index.md
  - Single files:
      - Page 1: chapter1/page1.md
      - Page 2: chapter1/page2.md
  - Project folder: ProjectFolder/index.md
include:
  - '*.md'
