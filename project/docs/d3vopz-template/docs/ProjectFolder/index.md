# Welcome to the project folder of d3vopz-library

This folder is the skeleton for d3vopz projects.

It comes with a predefined set of jobs, stages for gitlab pipeline `.gitlab-ci.yml`.

## `gitlab-ci.yml`

### jobs

| Key | Description |
| ---: | :--- |
| `changelog_create` | Creates a changelog based on the commits messages, if `CHANGELOG.md` does not exist. |
| `pages` | Creates the gitlab pages based on the content of directory specified with `D3VOPZ_MKDOCS_DIR`. |
| `kaniko_build` | Creates the docker registry images as targets specified in `D3VOPZ_TARGET_LIST`. |


### Variables

| Key | Description |
| ---: | :--- |
| `D3VOPZ_GITLABRUNNER_TAG` | Contains a local runner tag, so that not shared runners consuming credits. |
| `D3VOPZ_GITLAB_SCRIPT_TOKEN` | Contains the token for read access of d3vopz-script-base repository in job `changelog_create`. |
| `D3VOPZ_GITLAB_SCRIPT_USER` | Contains the user for read access of d3vopz-script-base repository in job `changelog_create`. |
| `D3VOPZ_MKDOCS_DIR` | Contains the path to the directory containing the mkdocs definition template `mkdocs.yml.tpl` and the `docs` folder containing markdown files. |
| `D3VOPZ_TARGET_LIST` | Contains the list of targets in `Dockerfile` to be build in job `kaniko_build`. |
| `D3VOPZ_DOCKER_DIR` | Contains the path to the docker build context and its `Dockerfile` |
| `D3VOPZ_IMAGE_USER` | Contains the name of the user in the created image build with job `kaniko_build` |
