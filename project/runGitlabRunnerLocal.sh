#!/bin/bash
: ${1?"Need to set \"d3vopz_gitlab_stage\""}
d3vopz_gitlab_stage="$1"
 echo "ddd: using d3vopz_gitlab_stage=\"${d3vopz_gitlab_stage}\""
if [ -z "${D3VOPZ_PROJECT_PATH}" ] || [ "x${D3VOPZ_PROJECT_PATH}" == "x" ]
then
 echo "www: using default D3VOPZ_PROJECT_PATH=\"${D3VOPZ_PROJECT_PATH}\""
 export D3VOPZ_PROJECT_PATH=.
else
 echo "iii: using D3VOPZ_PROJECT_PATH=\"${D3VOPZ_PROJECT_PATH}\""
fi
. ${D3VOPZ_PROJECT_PATH}/.setGitlabRunnerEnv.sh
. ${D3VOPZ_PROJECT_PATH}/.setGitlabRunnerCred.sh
. ${D3VOPZ_PROJECT_PATH}/.setGitlabRunnerEnv.sh
env | grep D3VOPZ_
env | grep CI_
docker kill gitlab-runner
docker run --rm -d --name gitlab-runner  -v $PWD:$PWD   -v /var/run/docker.sock:/var/run/docker.sock   gitlab/gitlab-runner:latest
docker exec -it -w $PWD gitlab-runner git config --global  --add safe.directory "$PWD"
docker exec -it -w $PWD gitlab-runner env
docker exec -it -w $PWD gitlab-runner gitlab-runner $D3VOPZ_GITLABRUNNER_DEBUG exec docker \
  -env "CI_REGISTRY_USER=$CI_REGISTRY_USER" \
  -env "CI_REGISTRY_PASSWORD=$CI_REGISTRY_PASSWORD" \
  -env "D3VOPZ_TARGET_LIST=$D3VOPZ_TARGET_LIST" \
  -env "D3VOPZ_USER_NAME=$D3VOPZ_USER_NAME" \
  -env "D3VOPZ_USER_GECOS=$D3VOPZ_USER_GECOS" \
  -env "CI_REGISTRY_IMAGE=$CI_REGISTRY_IMAGE" \
  -env "CI_REGISTRY=$CI_REGISTRY_IMAGE" \
  -env "CI_PROJECT_DIR=$CI_PROJECT_DIR" \
  -env "DOCKER_AUTH_CONFIG=$DOCKER_AUTH_CONFIG" \
  -env "D3VOPZ_GITLABRUNNER_TAG=$D3VOPZ_GITLABRUNNER_TAG" \
  $d3vopz_gitlab_stage
